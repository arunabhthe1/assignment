package com.paypal.bfs.test.employeeserv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.paypal.bfs.test.employeeserv.dao.EmployeesDao;
import com.paypal.bfs.test.employeeserv.impl.EmployeeResourceImpl;
import com.paypal.bfs.test.employeeserv.model.Address;
import com.paypal.bfs.test.employeeserv.model.Employee;

import junit.framework.Assert;


@RunWith(SpringRunner.class)
@WebMvcTest(value = EmployeeResourceImpl.class, secure = false)
public class EmployeeControllerTest {


    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EmployeesDao  employeesDao;
	
	@Test
	public void testGetEmployees() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Long id=1000l;
		Date date = new Date("18/09/2018");
		String formattedDate = dateFormat.format(date);
		Date dateOfBirth=null;
		dateOfBirth = dateFormat.parse(formattedDate);
		Employee mockEmployee = new Employee("Achi", "Dash", dateOfBirth);
		mockEmployee.setId(id);
		Address address=new Address("411 DSR Eden Green","Carmelaram","Bangalore","Karnataka","India","560035");
		address.setId(1001l);
		mockEmployee.setAddress(address);
		Optional<Employee> employeeDltOptional=Optional.of(mockEmployee);		
		Mockito.when(employeesDao.findById(Mockito.anyLong())).thenReturn(employeeDltOptional);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/v1/bfs/employees/"+id.toString()).accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		LOGGER.info("MOCK Response Test:"+result.getResponse().getContentAsString());
		String expected = "{\"id\": 1000,\"address\":{\"line1\":\"411 DSR Eden Green\",\"line2\":\"Carmelaram\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"zipcode\":\"560035\",\"id\":1001},\"firstName\":\"Achi\",\"lastName\":\"Dash\",\"dateOfBirth\":\"09/01/2019\"}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), JSONCompareMode.STRICT);
	}
	@Test
	public void testGetEmployeesWithStringId() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Long id=1000l;
		Date date = new Date("18/09/2018");
		String formattedDate = dateFormat.format(date);
		Date dateOfBirth=null;
		dateOfBirth = dateFormat.parse(formattedDate);
		Employee mockEmployee = new Employee("Achi", "Dash", dateOfBirth);
		mockEmployee.setId(id);
		Address address=new Address("411 DSR Eden Green","Carmelaram","Bangalore","Karnataka","India","560035");
		address.setId(1001l);
		mockEmployee.setAddress(address);
		Optional<Employee> employeeDltOptional=Optional.of(mockEmployee);		
		Mockito.when(employeesDao.findById(Mockito.anyLong())).thenReturn(employeeDltOptional);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/v1/bfs/employees/"+"abc").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		LOGGER.info("MOCK Response Test:"+result.getResponse().getContentAsString());
		String expected = "{\"errorMessage\":\"Please enter a valid number for getting the corresponding emplyee : Bad request presented : \"}";
		assertEquals(400, result.getResponse().getStatus());
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), JSONCompareMode.LENIENT);
		
	}
	
	@Test
	public void testSaveEmployeesWithValidationError() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Long id=1000l;
		Date date = new Date("18/09/2018");
		String formattedDate = dateFormat.format(date);
		Date dateOfBirth=null;
		dateOfBirth = dateFormat.parse(formattedDate);
		Employee mockEmployee = new Employee("Achi", "Dash", dateOfBirth);
		mockEmployee.setId(id);
		Address address=new Address("411 DSR Eden Green","Carmelaram","Bangalore","Karnataka","India","560035");
		address.setId(1001l);
		mockEmployee.setAddress(address);
		
		Mockito.when(employeesDao.save(Mockito.any(Employee.class))).thenReturn(mockEmployee);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("http://localhost:8080/v1/bfs/employees/")
				.content("{\"address\":{\"line1\":\"411 DSR Eden Green\",\"line2\":\"Carmelaram\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"zip_code\":\"560035\",\"id\":null},\"first_name\":\"Achi\",\"last_name\":\"Dash\",\"date_of_birth\":\"09/01/2019\"}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON);
		
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		LOGGER.info("MOCK Response Test:"+result.getResponse().getContentAsString());
		assertEquals(400, result.getResponse().getStatus());
		
		String expected = "{\"errorMessage\":\"Please fix your json $.address.zip_code: string found, number expected\\n : Bad request presented : \"}";;
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), JSONCompareMode.LENIENT);
		
	}
	@Test
	public void testSaveEmployeesWithValidationFirstNameError() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Long id=1000l;
		Date date = new Date("18/09/2018");
		String formattedDate = dateFormat.format(date);
		Date dateOfBirth=null;
		dateOfBirth = dateFormat.parse(formattedDate);
		Employee mockEmployee = new Employee("Achi", "Dash", dateOfBirth);
		mockEmployee.setId(id);
		Address address=new Address("411 DSR Eden Green","Carmelaram","Bangalore","Karnataka","India","560035");
		address.setId(1001l);
		mockEmployee.setAddress(address);
		
		Mockito.when(employeesDao.save(Mockito.any(Employee.class))).thenReturn(mockEmployee);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("http://localhost:8080/v1/bfs/employees/")
				.content("{\"address\":{\"line1\":\"411 DSR Eden Green\",\"line2\":\"Carmelaram\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"zip_code\":560035,\"id\":null},\"date_of_birth\":\"09/01/2019\"}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON);
		
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		LOGGER.info("MOCK Response Test:"+result.getResponse().getContentAsString());
		assertEquals(400, result.getResponse().getStatus());
		
		String expected = "{\"errorMessage\":\"Please fix your json $.first_name: is missing but it is required\\n$.last_name: is missing but it is required\\n : Bad request presented : \"}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), JSONCompareMode.LENIENT);
		
	}
}
