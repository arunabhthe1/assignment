package com.paypal.bfs.test.employeeserv.exceptionhandling;

public class EmployeeCustomExeption extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public EmployeeCustomExeption(String message) {
		super(message);
	}

}
