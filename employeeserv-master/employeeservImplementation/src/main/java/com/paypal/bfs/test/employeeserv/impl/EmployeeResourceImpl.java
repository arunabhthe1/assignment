package com.paypal.bfs.test.employeeserv.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import com.paypal.bfs.test.employeeserv.api.EmployeeResource;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dao.EmployeesDao;
import com.paypal.bfs.test.employeeserv.exceptionhandling.EmployeeNotFoundExeption;
import com.paypal.bfs.test.employeeserv.exceptionhandling.EmployeeValidationExeption;
import com.paypal.bfs.test.employeeserv.exceptionhandling.EmployeeCustomExeption;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implementation class for employee resource.
 */
@RestController
public class EmployeeResourceImpl implements EmployeeResource {
	@Autowired
	EmployeesDao emplyeesDao;
	ObjectMapper mapper = new ObjectMapper();
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    @Override
	public ResponseEntity<Employee> employeeGetById(String id) {
		Employee employee = null;
		try {
			Optional<com.paypal.bfs.test.employeeserv.model.Employee> optinalEmp = emplyeesDao
					.findById(Long.parseLong(id));
			String jsonStr = mapper.writeValueAsString(optinalEmp
					.orElseThrow(() -> new EmployeeNotFoundExeption("The Employee with id " + id + " is not found")));
			employee = mapper.readValue(jsonStr, Employee.class);
		} catch (NumberFormatException e) {
			LOGGER.error("Please enter a valid number for getting the corresponding emplyee", e);
			throw new EmployeeValidationExeption("Please enter a valid number for getting the corresponding emplyee");
		} catch (JsonProcessingException e) {
			LOGGER.error("Not able to parse the data present in the database for this employee", e);
			throw new EmployeeCustomExeption("Not able to parse the data present in the database for this employee");
		} catch (IOException e) {
			LOGGER.error("Getting exception while marshalling the json to emplyee object", e);
			throw new EmployeeCustomExeption("Getting exception while marshalling the json to emplyee object");
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}


	@Override
	public ResponseEntity<Employee> saveEmployeeWithValidation(String jsonValue)  {
    	LOGGER.info("Json Data"+jsonValue);
    	InputStream schemaAsStream=EmployeeResourceImpl.class.getClassLoader().getResourceAsStream("v1/schema/employee.json");
    	JsonSchema schema=JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7).getSchema(schemaAsStream);
    	ObjectMapper mapper = new ObjectMapper();
    	Set<ValidationMessage> errors=null;
    	Employee employee=null;
    	String errorCombined="";
    	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Date dob = null;
    	try {
			JsonNode jsonNode=mapper.readTree(jsonValue);
			errors=schema.validate(jsonNode);
			for(ValidationMessage error:errors) {
				LOGGER.error("validation Errors {}",error);
				errorCombined+=error.toString()+ "\n";
			}
			if(errors.size()>0) {
	    		throw new EmployeeValidationExeption("Please fix your json "+errorCombined);
	    	}
			employee= mapper.readValue(jsonValue, Employee.class);
			dob = sdf.parse(employee.getDateOfBirth()); 
		} catch (ParseException e) {
			LOGGER.error("Date format is not in dd/MM/yyyy format. Please enter the correct format",e);
	    	throw new EmployeeValidationExeption("Date format is not in dd/mm/yyyy format. Please enter the correct format");
		}catch (IOException e) {
			LOGGER.error("Getting exception while marshalling the json to emplyee object",e);
	    	throw new EmployeeCustomExeption("Getting exception while marshalling the json to emplyee object");
		}

		com.paypal.bfs.test.employeeserv.model.Employee emp=new com.paypal.bfs.test.employeeserv.model.Employee(employee.getFirstName(),employee.getLastName(),dob);
		com.paypal.bfs.test.employeeserv.model.Address address=new com.paypal.bfs.test.employeeserv.model.Address(employee.getAddress().getLine1(),employee.getAddress().getLine2(),employee.getAddress().getCity(),employee.getAddress().getState(),employee.getAddress().getCountry(),employee.getAddress().getZipCode().toString());        
		emp.setAddress(address);
		emplyeesDao.save(emp);
		employee.setId(emp.getId().intValue());
        return new ResponseEntity<>(employee, HttpStatus.OK);
   	}
}
