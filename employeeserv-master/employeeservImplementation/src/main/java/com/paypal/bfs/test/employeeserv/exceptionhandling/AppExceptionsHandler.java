package com.paypal.bfs.test.employeeserv.exceptionhandling;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
@ControllerAdvice
public class AppExceptionsHandler extends ResponseEntityExceptionHandler{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	

	@ExceptionHandler(value={EmployeeNotFoundExeption.class})
	public ResponseEntity<Object> handleDBException(EmployeeNotFoundExeption ex, WebRequest request){
		LOGGER.error("Not getting the entity you are looking for ",ex);
		ErrorMessage errorMessage=new ErrorMessage(ex.getLocalizedMessage()+" : The entity which your are looking for in not found" ,new Date());
		
		return new ResponseEntity<Object>(errorMessage,new HttpHeaders(),HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value={EmployeeCustomExeption.class})
	public ResponseEntity<Object> handleDBException(EmployeeCustomExeption ex, WebRequest request){
		LOGGER.error("There is something went wrong . Please get in touch with support ",ex);
		ErrorMessage errorMessage=new ErrorMessage(ex.getLocalizedMessage()+" : There is something went wrong . Please get in touch with support" ,new Date());
		
		return new ResponseEntity<Object>(errorMessage,new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value={EmployeeValidationExeption.class})
	public ResponseEntity<Object> handleDBException(EmployeeValidationExeption ex, WebRequest request){
		LOGGER.error("Bad request presented ",ex);
		ErrorMessage errorMessage=new ErrorMessage(ex.getLocalizedMessage()+" : Bad request presented : ",new Date());
		
		return new ResponseEntity<Object>(errorMessage,new HttpHeaders(),HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(value={Exception.class})
	public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request){
		LOGGER.error("Getting exeption while processing ",ex);
		ErrorMessage errorMessage=new ErrorMessage(ex.getLocalizedMessage()+" : This is the custom Message" ,new Date());
		
		return new ResponseEntity<Object>(errorMessage,new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	

}
