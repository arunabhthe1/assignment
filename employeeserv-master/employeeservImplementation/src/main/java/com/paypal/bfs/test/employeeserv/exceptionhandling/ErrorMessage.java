package com.paypal.bfs.test.employeeserv.exceptionhandling;

import java.util.Date;

public class ErrorMessage {
	
	private String errorMessage;
	private Date date;
	public ErrorMessage(String errorMessage,Date date){
		this.errorMessage=errorMessage;
		this.date=date;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
