package com.paypal.bfs.test.employeeserv.exceptionhandling;

public class EmployeeNotFoundExeption extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public EmployeeNotFoundExeption(String message) {
		super(message);
	}

}
