package com.paypal.bfs.test.employeeserv.exceptionhandling;

public class EmployeeValidationExeption extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public EmployeeValidationExeption(String message) {
		super(message);
	}

}
