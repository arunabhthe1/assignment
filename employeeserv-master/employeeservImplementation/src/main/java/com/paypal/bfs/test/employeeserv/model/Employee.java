package com.paypal.bfs.test.employeeserv.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "EMPLOYEE")
@Getter @Setter 
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="Employee_SEQ")
	private Long id;
    private String firstName;
    private String lastName;
    @JsonFormat(pattern = "DD/MM/YYYY")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name="ADDRESS_ID")
    private Address address;
    public Employee() {
    	
    }
    public Employee(String firstName, String lastName, Date  dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    @JsonFormat(pattern = "DD/MM/YYYY")
	public String getDateOfBirth() {
    	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(this.dateOfBirth);
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
